﻿using Autofac;
using Bosplant.Core.Enums;
using Bosplant.Core.Infrastructure.IOC;
using Bosplant.Core.Model;
using Bosplant.Core.ViewModels;
using Bosplant.Data.Infrastructure.IOC;
using Bosplant.Services.Interfaces;
using Bosplant.Services.Logging;
using Bosplant.SS.LadleBubbling.ServiceHost.DTO;
using Bosplant.SS.LadleBubbling.ServiceHost.Processor;
using Bosplant.SS.LadleBubbling.ServiceHost.Services;
using MLJSystems.Pi;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.Autofac;
using Topshelf.Quartz;

namespace Bosplant.SS.LadleBubbling.ServiceHost
{
    /*
    public class MyJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine($"[{DateTime.UtcNow}] Welcome from MyJob!");
        }
    }*/

    class Program
    {
        static void Main(string[] args)
        {
            FLog.LogInformation("Initialising Bosplant.SS.LadleBubbling.ServiceHost ...");

            IContainer container = GetContainer();

            var rc = HostFactory.Run(x =>
            {
                x.UseAutofacContainer(container);

                x.Service<LadleBubblingService>(s =>
                {
                    s.ConstructUsingAutofacContainer();

                    /*
                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() =>
                            JobBuilder.Create<MyJob>().Build())
                            .AddTrigger(() => TriggerBuilder.Create()
                                .WithSimpleSchedule(b => b
                                    .WithIntervalInSeconds(10)
                                    .RepeatForever())
                                .Build()));
                                */
                    s.WhenStarted((service, hostControl) =>
                    {
                        return service.Start(hostControl);
                    });
                    s.WhenStopped((service, hostControl) =>
                    {
                        return service.Stop(hostControl);
                    });
                    s.WhenShutdown((service, hostControl) => service.Stop(hostControl));
                });

                x.EnableServiceRecovery(src =>
                {
                    // Has no corresponding setting in the Recovery dialogue.
                    // OnCrashOnly means the service will not restart if the application returns
                    // a non-zero exit code.  By convention, an exit code of zero means ‘success’.
                    src.OnCrashOnly();

                    // first failure: restart immediately
                    src.RestartService(delayInMinutes: 0);

                    // second failure: restart after 1 minute
                    src.RestartService(delayInMinutes: 1);

                    // at the moment we do nothing after subsequent failures as its not been set

                    // Corresponds to ‘Subsequent failures: Restart the Service’
                    // ie: src.RestartService(delayMinutes: 5);

                    // Corresponds to 'Reset fail cout after: 1 days'
                    src.SetResetPeriod(days: 1);
                });

                x.EnableShutdown();

                x.RunAsLocalSystem();

                x.SetDisplayName("Bosplant Ladle Bubbling");
                x.SetDescription("Ladle bubbling");
                x.SetServiceName("Bosplant.SS.LadleBubbling");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }

        private static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<CoreModule>();
            builder.RegisterModule(
                new DataModule(
                    psmetConnectionString: System.Configuration.ConfigurationManager.ConnectionStrings["PSMETConnection"].ConnectionString));
            builder.RegisterType<LadleBubblingService>();
            builder.RegisterType<LadleBubblingCAS1Processor>().As<ILadleBubblingCAS1Processor>();
            builder.RegisterType<LadleBubblingCAS2Processor>().As<ILadleBubblingCAS2Processor>();
            builder.RegisterType<LadleBubblingLanceProcessor>().As<ILadleBubblingLanceProcessor>();            
            builder.RegisterType<LadleBubblingRDProcessor>().As<ILadleBubblingRDProcessor>();
            builder.RegisterType<LadleBubblingRHProcessor>().As<ILadleBubblingRHProcessor>();
            builder.RegisterType<LadleBubblingV1Processor>().As<ILadleBubblingV1Processor>();
            builder.RegisterType<LadleBubblingV2Processor>().As<ILadleBubblingV2Processor>();

            return builder.Build();
        }
    }
}
