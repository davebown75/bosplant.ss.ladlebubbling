﻿using Bosplant.Services.Logging;
using Bosplant.SS.LadleBubbling.ServiceHost.Processor;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;

namespace Bosplant.SS.LadleBubbling.ServiceHost.Services
{
    public class LadleBubblingService : ServiceControl
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly CancellationToken _cancellationToken;
        private ILadleBubblingCAS1Processor _ladleBubblingCAS1Processor;
        private ILadleBubblingCAS2Processor _ladleBubblingCAS2Processor;
        private ILadleBubblingLanceProcessor _ladleBubblingLanceProcessor;
        private ILadleBubblingRDProcessor _ladleBubblingRDProcessor;
        private ILadleBubblingRHProcessor _ladleBubblingRHProcessor;
        private ILadleBubblingV1Processor _ladleBubblingV1Processor;
        private ILadleBubblingV2Processor _ladleBubblingV2Processor;
        private HostControl _hostControl;

        public LadleBubblingService(
            ILadleBubblingCAS1Processor ladleBubblingCAS1Processor,
            ILadleBubblingCAS2Processor ladleBubblingCAS2Processor,
            ILadleBubblingLanceProcessor ladleBubblingLanceProcessor,            
            ILadleBubblingRDProcessor ladleBubblingRDProcessor,
            ILadleBubblingRHProcessor ladleBubblingRHProcessor,
            ILadleBubblingV1Processor ladleBubblingV1Processor,
            ILadleBubblingV2Processor ladleBubblingV2Processor
            )
        {
            _ladleBubblingCAS1Processor = ladleBubblingCAS1Processor;
            _ladleBubblingCAS2Processor = ladleBubblingCAS2Processor;
            _ladleBubblingLanceProcessor = ladleBubblingLanceProcessor;
            _ladleBubblingRDProcessor = ladleBubblingRDProcessor;
            _ladleBubblingRHProcessor = ladleBubblingRHProcessor;
            _ladleBubblingV1Processor = ladleBubblingV1Processor;
            _ladleBubblingV2Processor = ladleBubblingV2Processor;

            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
            Task.Run(() => (DoWork()), _cancellationToken);
        }

        async Task DoWork()
        {
            while(!_cancellationToken.IsCancellationRequested)
            {
                try
                {
                    // await _ladleBubblingProcessor.Process();
                    // await _ladleBubblingCAS1Processor.Process();
                    // await _ladleBubblingCAS2Processor.Process();
                    // await _ladleBubblingLanceProcessor.Process();
                    // await _ladleBubblingRDProcessor.Process();
                    // await _ladleBubblingRHProcessor.Process();
                    // await _ladleBubblingV1Processor.Process();
                    await _ladleBubblingV2Processor.Process();
                }
                catch(Exception ex)
                {
                    await Task.Delay(TimeSpan.FromSeconds(10), _cancellationToken);
                    LogUnhandledException(exception: ex, errorType: "UnhandledException");
                }
                finally
                {
                    await Task.Delay(TimeSpan.FromSeconds(5), _cancellationToken);
                }
            }
        }

        private void LogUnhandledException(Exception exception, string errorType, string @logType = "Error")
        {
            var additionalInfo = new Dictionary<string, object>();

            additionalInfo["ErrorType"] = errorType;

            var logDetail = FLog.GetLogEntry(exception.GetBaseException().Message, additionalInfo, "Bosplant.SS.LadleBubbling.ServiceHost", exception);
            FLog.Write(logDetail);
        }

        public bool Start(HostControl hostControl)
        {
            Log.Information("Ladle bubbling task started.");
            _hostControl = hostControl;

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Log.Information("Ladle bubbling task stopping ...");

            // we cancel the task in the stop() method
            if (!_cancellationTokenSource.IsCancellationRequested)
                _cancellationTokenSource.Cancel();

            return true;
        }
    }
}
