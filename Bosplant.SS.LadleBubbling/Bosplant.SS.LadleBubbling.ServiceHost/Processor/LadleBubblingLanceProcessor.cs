﻿using Bosplant.Core.Enums;
using Bosplant.Core.Model.PSMET;
using Bosplant.Data.Interfaces;
using Bosplant.Data.Interfaces.PSMET;
using Bosplant.SS.LadleBubbling.ServiceHost.DTO;
using MLJSystems.Pi;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosplant.SS.LadleBubbling.ServiceHost.Processor
{
    public class LadleBubblingLanceProcessor : ILadleBubblingLanceProcessor
    {
        private readonly ILadleBubblingLanceRepository _ladleBubblingLanceRepository;
        private static MLJSystems.Pi.PI _pi;

        public LadleBubblingLanceProcessor(ILadleBubblingLanceRepository ladleBubblingLanceRepository)
        {
            _ladleBubblingLanceRepository = ladleBubblingLanceRepository;

            try
            {
                if (_pi == null)
                    _pi = new PI("ps_pi");

                Log.Information("Connected to PS PI.. ok");
            }
            catch
            {
                // If we can't connect to PI this service won't do much, 
                // but we can keep running and trying to reconnect. 
                // We will continue to connect when we try to UpdatePi below. 
                Log.Error("Unable to connect to PI Server in PiService constructor.");
            }
        }

        public bool IsConnected()
        {
            if (_pi == null)
                _pi = new PI("ps_pi");

            return _pi != null;
        }

        private LadleBubblingLanceParams cas1Tags = new LadleBubblingLanceParams
        {
            Unit = LadleUnit.CAS_1_LOCATION,
            LadleLimitTag = "GM08G28_9",
            LadleLimitTagFilter = "('GM08G28_9'=\"TRUE\" )",
            CarAtUnitFilter = "",
            FlowTag = "GM08G954",
            FlowTagFilter = "('GM08G954'>50)",
            HoodDownTag = "GM08H212",
            HoodDownTagFilter = "('GM08H212'<5000)",
            CouplerEngagedTag = "Cas1_Coupler Engaged",
            CouplerEngagedTagFilter = "('Cas1_Coupler_Line_1_Flow_Setpoint' > 25 or 'Cas1_Coupler_Line_2_Flow_Setpoint' > 25)",
            HeatTag = "X_HEAT_CAS",
            HeatTagFilter = "'X_HEAT_CAS'>0",
            ActualFlowTag = "Cas1_Coupler_Line_1_Actual_Flow"
        };

        private LadleBubblingLanceParams cas2Tags = new LadleBubblingLanceParams
        {
            Unit = LadleUnit.CAS_2_LOCATION,
            LadleLimitTag = "CAS2_N20_D_M3050_WLIM",
            LadleLimitTagFilter = "('CAS2_N20_D_M3050_WLIM' =0)",
            CarAtUnitFilter = "('CAS2_N20_D_M3050_WLIM' =-1)",
            FlowTag = "CAS2_N20_R_FI14018",
            FlowTagFilter = "('CAS2_N20_R_FI14018'>50)",
            ActualFlowTag = "Cas2_Coupler_Line_1_Actual_Flow",
            ActualFlowTagFilter = "('Cas2_Coupler_Line_1_Actual_Flow'>20)",
            NextCarPositionTag = "CAS2_N20_M3050_ELIM",
            NextCarPositionTagFilter = "('CAS2_N20_M3050_ELIM'=0 or 'CAS2_N20_M3050_WLIM'=0)",
            HeatTag = "X_HEAT_CAS2",
            HeatTagFilter = "('X_HEAT_CAS2'>0)"
        };

        public async Task Process()
        {
            var cas1List = await CalculateLadleBubblingLanceAtCas1(_pi, cas1Tags);
            var cas2List = await CalculateLadleBubblingLanceAtCas2(_pi, cas2Tags);

            var allUnits = cas1List.Concat(cas2List).ToList();

            await SaveLadleBubblingLance(allUnits);
        }

        private async Task<List<LadleBubblingLance>> CalculateLadleBubblingLanceAtCas1(PI pi, LadleBubblingLanceParams ladleParams)
        {
            var ladleBubblingLance = await _ladleBubblingLanceRepository.GetLastLadleBubblingLanceAtUnit(ladleParams.Unit);

            // var startDate = new DateTime(2018, 08, 12, 07, 00, 00); //ladleBubblingLance.LanceEngagedTime ?? DateTime.Now.AddDays(-1);
            // var endDate = new DateTime(2018, 08, 13, 15, 23, 33);// DateTime.Now;

            var startDate = new DateTime(2018, 09, 09, 07, 0, 0);// ladleBubbling.EventTime ?? DateTime.Now.AddDays(-1);
            var endDate = new DateTime(2018, 09, 16, 19, 0, 0);


            var ladleBubblingLances = new List<LadleBubblingLance>();
            LadleBubblingLance staging;
            var lanceEngaged = pi.GetRecordedValuesByInside(ladleParams.LadleLimitTag, startDate, endDate, ladleParams.LadleLimitTagFilter);

            if (lanceEngaged.Count < 2)
                return ladleBubblingLances;

            for (int i = 0; i < lanceEngaged.Count - 1; i++)
            {
                staging = new LadleBubblingLance
                {
                    Unit = (int)ladleParams.Unit,
                    IsLanceEngaged = lanceEngaged[i].Bool,
                    LanceEngagedTime = lanceEngaged[i].Timestamp
                };

                if (staging.IsLanceEngaged)
                {
                    /*
                    var cas1NextBubbleTimetest = pi.GetRecordedValuesByCount(ladleParams.FlowTag, staging.LanceEngagedTime.Value.AddMinutes(-1), ladleParams.FlowTagFilter, false, 10, Direction.dForward, BoundaryType.btInside)
                                    .OrderBy(s => s.Timestamp).ToList();

                    var cas1LastBubbleTimetest = pi.GetRecordedValuesByCount(ladleParams.FlowTag, lanceEngaged[i + 1].Timestamp.AddMinutes(1), ladleParams.FlowTagFilter, count: 10, direction: Direction.dReverse, boundaryType: BoundaryType.btInside)
                                    .OrderByDescending(s => s.Timestamp).ToList();
                    */

                    var cas1NextBubble = pi.GetRecordedValuesByCount(ladleParams.FlowTag, staging.LanceEngagedTime.Value.AddMinutes(-1), ladleParams.FlowTagFilter, false, 10, Direction.dForward, BoundaryType.btInside)
                        .OrderBy(s => s.Timestamp)
                        .FirstOrDefault();

                    if(cas1NextBubble != null)
                    {
                        staging.StartBubblingTime = cas1NextBubble.Timestamp;
                        staging.StartBubblingValue = cas1NextBubble.Float;
                    }

                    var cas1LastBubble = pi.GetRecordedValuesByCount(ladleParams.FlowTag, lanceEngaged[i + 1].Timestamp.AddMinutes(1), ladleParams.FlowTagFilter, count: 10, direction: Direction.dReverse, boundaryType: BoundaryType.btInside)
                                    .OrderByDescending(s => s.Timestamp)
                                    .FirstOrDefault();
                                    
                    if(cas1LastBubble != null)
                    { 
                        staging.EndBubblingTime = cas1LastBubble.Timestamp;
                        staging.EndBubblingValue = cas1LastBubble.Float;
                    }

                    var cas1HoodDownTime = pi.GetRecordedValuesByCount(ladleParams.HoodDownTag, staging.LanceEngagedTime.Value.AddMinutes(-1), ladleParams.HoodDownTagFilter, false, 10)
                                    .OrderBy(s => s.Timestamp)
                                    .Select(s => s.Timestamp)
                                    .FirstOrDefault();

                    if(cas1HoodDownTime != DateTime.MinValue)
                    { 
                        staging.HoodDownTime = cas1HoodDownTime;
                    }

                    var cas1NextCoupler = pi.GetRecordedValuesByCount(ladleParams.CouplerEngagedTag, staging.LanceEngagedTime.Value.AddMinutes(-1), ladleParams.CouplerEngagedTagFilter, false, 10)
                                    .OrderBy(s => s.Timestamp)
                                    .FirstOrDefault();

                    if(cas1NextCoupler != null )
                    { 
                        staging.StartBubblingCouplerTime = cas1NextCoupler.Timestamp;
                        staging.StartBubblingCouplerValue = cas1NextCoupler.Int;
                    }

                    var cas1LastCoupler = pi.GetRecordedValuesByCount(ladleParams.CouplerEngagedTag, lanceEngaged[i + 1].Timestamp.AddMinutes(1), ladleParams.CouplerEngagedTagFilter, false, 10, Direction.dReverse, BoundaryType.btInside)
                                    .OrderByDescending(s => s.Timestamp)
                                    .FirstOrDefault();

                    if(cas1LastCoupler != null)
                    { 
                        staging.EndBubblingCouplerTime = cas1LastCoupler.Timestamp;
                        staging.EndBubblingCouplerValue = cas1LastCoupler.Int;
                    }
                    
                    var cas1StartTreatment = new[] { staging.StartBubblingTime, staging.HoodDownTime, staging.StartBubblingCouplerTime }
                        .Where(s => s.HasValue && s.Value != DateTime.MinValue)
                        .Min();

                    staging.StartTreatmentTimeStaging = cas1StartTreatment;

                    staging.HeatTime = pi.GetRecordedValuesByCount(ladleParams.HeatTag, lanceEngaged[i + 1].Timestamp, ladleParams.HeatTagFilter, false, 10, Direction.dReverse, BoundaryType.btInside)
                                    .OrderByDescending(s => s.Timestamp)
                                    .Select(s => s.Timestamp)
                                    .FirstOrDefault();
                    
                    var lastHeat = pi.GetRecordedValuesByCount(ladleParams.HeatTag, lanceEngaged[i + 1].Timestamp, ladleParams.HeatTagFilter, false, 10, Direction.dReverse, BoundaryType.btInside)
                                    .OrderByDescending(s => s.Timestamp)
                                    .Select(s => s.String)
                                    .FirstOrDefault();

                    if(lastHeat.Length == 5 && 
                        (staging.HeatTime.HasValue && staging.HeatTime.Value.AddMinutes(3) >= staging.LanceEngagedTime) && 
                        staging.HeatTime <= lanceEngaged[i+1].Timestamp)
                    {
                        staging.HeatNumberAtEnd = int.Parse(lastHeat);
                    }
                    else
                    {
                       staging.HeatNumberAtEnd  = pi.GetIntArchive(ladleParams.HeatTag, lanceEngaged[i + 1].Timestamp);
                    }

                    if (staging.HeatNumberAtEnd != null && 
                        staging.StartTreatmentTimeStaging < lanceEngaged[i + 1].Timestamp)
                    {
                        staging.StartTreatmentTime = staging.StartTreatmentTimeStaging;
                    }

                    if (staging.StartTreatmentTime.HasValue && 
                        staging.StartTreatmentTime.Value != DateTime.MinValue && 
                        (staging.EndBubblingCouplerTime.HasValue || staging.EndBubblingTime.HasValue))
                    {
                        staging.EndTreatmentTime = new DateTime?[] { staging.EndBubblingCouplerTime, staging.EndBubblingTime }.Max();
                        staging.HasLanceFlow = pi.GetMax(ladleParams.FlowTag, staging.StartTreatmentTime.Value, staging.EndTreatmentTime.Value) > 50;
                        staging.HasCouplerFlow = pi.GetMax(ladleParams.ActualFlowTag, staging.StartTreatmentTime.Value, staging.EndTreatmentTime.Value) > 20;
                    }
                }
                ladleBubblingLances.Add(staging);
            }            
            return ladleBubblingLances;
        }


        private async Task<List<LadleBubblingLance>> CalculateLadleBubblingLanceAtCas2(PI pi, LadleBubblingLanceParams ladleParams)
        {
            var ladleBubblingLance = await _ladleBubblingLanceRepository.GetLastLadleBubblingLanceAtUnit(ladleParams.Unit);

            var startDate = new DateTime(2018, 08, 12, 07, 00, 00); //ladleBubblingLance.LanceEngagedTime ?? DateTime.Now.AddDays(-1);
            var endDate = new DateTime(2018, 08, 13, 15, 23, 33); // DateTime.Now;

            var ladleBubblingLances = new List<LadleBubblingLance>();
            LadleBubblingLance staging;

            // good
            var lanceEngaged = pi.GetRecordedValuesByInside(ladleParams.LadleLimitTag, startDate, endDate, ladleParams.LadleLimitTagFilter, true);

            if (lanceEngaged.Count < 2)
                return ladleBubblingLances;

            for (int i = 0; i < lanceEngaged.Count - 1; i++)
            {
                staging = new LadleBubblingLance
                {
                    Unit = (int)ladleParams.Unit,
                    IsLanceEngaged = lanceEngaged[i].Bool,
                    LanceEngagedTime = lanceEngaged[i].Timestamp
                };

                // new
                if (staging.IsLanceEngaged) // || (!staging.IsLanceEngaged && lanceEngaged[i + 1].Bool))
                {                   
                    var cas2CarAtUnit = pi.GetRecordedValuesByCount(ladleParams.LadleLimitTag, staging.LanceEngagedTime.Value, ladleParams.CarAtUnitFilter, false, 10)
                                    .OrderBy(s => s.Timestamp)
                                    .FirstOrDefault();

                    if(cas2CarAtUnit != null)
                    {
                        staging.CarAtUnitTime = cas2CarAtUnit.Timestamp;
                        staging.CarAtUnitValue = cas2CarAtUnit.Float;
                    }

                    if (staging.CarAtUnitValue == -1)
                    {
                        var cas2StartBubbling = pi.GetRecordedValuesByCount(ladleParams.FlowTag, staging.CarAtUnitTime.Value.AddMinutes(-1), ladleParams.CarAtUnitFilter, false, 10)
                                                    .OrderBy(s => s.Timestamp)
                                                    .FirstOrDefault();

                        if(cas2StartBubbling != null)
                        {
                            staging.StartBubblingTime = cas2StartBubbling.Timestamp;
                            staging.StartBubblingValue = cas2StartBubbling.Float;
                        }

                        var cas2StartBubblingCoupler = pi.GetRecordedValuesByCount(ladleParams.ActualFlowTag, cas2CarAtUnit.Timestamp.AddMinutes(-1), ladleParams.ActualFlowTagFilter, false, 10)
                                                            .OrderBy(s => s.Timestamp)
                                                            .FirstOrDefault();

                        staging.StartBubblingCouplerTime = cas2StartBubblingCoupler.Timestamp;
                        staging.StartBubblingCouplerValue = cas2StartBubblingCoupler.Float;
                        staging.StartTreatmentTimeStaging = new[] { staging.StartBubblingTime, staging.StartBubblingCouplerTime }.Min();
                        staging.IsValidStartTreatmentTime = (staging.StartTreatmentTimeStaging < lanceEngaged[i + 2].Timestamp);
                    }

                    if(staging.IsValidStartTreatmentTime == true)
                    {                        
                        var cas2NextCarPos = pi.GetRecordedValuesByCount(ladleParams.NextCarPositionTag, staging.StartTreatmentTimeStaging.Value, ladleParams.NextCarPositionTagFilter, false, 10)
                                                            .OrderBy(s => s.Timestamp)
                                                            .FirstOrDefault();

                        if(cas2NextCarPos !=null)
                        {
                            staging.NextCarPositionTime = cas2NextCarPos.Timestamp;
                        }

                        var cas2WLimit = pi.GetRecordedValuesByCount(ladleParams.LadleLimitTag, staging.StartTreatmentTimeStaging.Value, ladleParams.NextCarPositionTagFilter, false, 10)
                                                            .OrderBy(s => s.Timestamp)
                                                            .FirstOrDefault();

                        DateTime? cas2CarAway = null;

                        if(cas2WLimit != null && staging.NextCarPositionTime != null)
                        {
                            cas2CarAway = new[] { staging.NextCarPositionTime, cas2WLimit.Timestamp }.Min();

                            // FlowTag
                            var cas2LastBubbleTime = pi.GetRecordedValuesByCount(ladleParams.FlowTag, cas2CarAway.Value.AddMinutes(1), ladleParams.FlowTagFilter, false, 10)
                                            .OrderByDescending(s => s.Timestamp)
                                            .FirstOrDefault();

                            if(cas2LastBubbleTime != null)
                            {
                                staging.EndBubblingTime = cas2LastBubbleTime.Timestamp;
                                staging.EndBubblingValue = cas2LastBubbleTime.Float;
                            }

                            var cas2LastCoupler = pi.GetRecordedValuesByCount(ladleParams.ActualFlowTag, cas2CarAway.Value, ladleParams.ActualFlowTagFilter, false, 10)
                                    .OrderByDescending(s => s.Timestamp)
                                    .FirstOrDefault();

                            if(cas2LastCoupler != null)
                            {
                                staging.EndBubblingCouplerTime = cas2LastCoupler.Timestamp;
                                staging.EndBubblingCouplerValue = cas2LastCoupler.Float;
                            }

                            staging.EndTreatmentTimeStaging = new DateTime?[] { staging.EndBubblingCouplerTime, staging.EndBubblingTime }.Max();

                            if(staging.EndBubblingTime.HasValue)
                            {
                                var cas2Lance = (staging.EndBubblingTime > staging.StartTreatmentTime && staging.EndBubblingTime.Value <= staging.EndTreatmentTimeStaging);

                                var cas2Coupler = staging.EndBubblingTime > staging.StartTreatmentTimeStaging && staging.EndBubblingCouplerTime <= staging.EndTreatmentTime && 
                                    staging.EndBubblingCouplerTime > (staging.StartBubblingCouplerTime.Value.AddMinutes(2));
                            }
                        }
                    }

                    if (lanceEngaged[i + 2] == null || (staging.StartTreatmentTimeStaging < lanceEngaged[i + 2].Timestamp))
                    {
                        staging.StartTreatmentTime = staging.StartTreatmentTimeStaging;
                    }
                    
                    if(staging.StartTreatmentTime.HasValue)
                    {
                        staging.EndTreatmentTime = staging.EndTreatmentTimeStaging;
                        staging.HeatAtStartTreatment = pi.GetIntArchive(ladleParams.HeatTag, staging.StartTreatmentTime.Value);
                    }

                    if(staging.EndTreatmentTime.HasValue)
                    {
                        staging.HeatDuringTreatment = pi.GetIntArchive(ladleParams.HeatTag, staging.EndTreatmentTime.Value);
                    }

                    staging.ActualHeat = (staging.HeatAtStartTreatment != null) ? staging.HeatAtStartTreatment : staging.HeatDuringTreatment;

                    if (staging.EndTreatmentTime != null)
                    {                        
                        var cas2HeatTime = pi.GetRecordedValuesByCount(ladleParams.HeatTag, staging.EndTreatmentTime.Value, ladleParams.HeatTagFilter, false, 10)
                                    .OrderByDescending(s => s.Timestamp)
                                    .FirstOrDefault();

                        if(cas2HeatTime != null)
                        {
                            staging.HeatTime = cas2HeatTime.Timestamp;
                        }
                    }                        
                }
                ladleBubblingLances.Add(staging);
            }
            return ladleBubblingLances;
        }

        private async Task SaveLadleBubblingLance(List<LadleBubblingLance> ladleBubblingLance)
        {
            await _ladleBubblingLanceRepository.InsertAsync(ladleBubblingLance);
        }
    }
}
