﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosplant.SS.LadleBubbling.ServiceHost.Processor
{
    public interface ILadleBubblingV2Processor
    {
        Task Process();
    }
}
