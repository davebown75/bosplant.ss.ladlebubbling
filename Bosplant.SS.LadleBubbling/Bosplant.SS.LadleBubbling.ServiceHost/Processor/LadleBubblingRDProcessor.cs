﻿using Bosplant.Core.Enums;
using Bosplant.Data.Interfaces;
using Bosplant.Data.Interfaces.PSMET;
using Bosplant.SS.LadleBubbling.ServiceHost.DTO;
using MLJSystems.Pi;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosplant.SS.LadleBubbling.ServiceHost.Processor
{
    public class LadleBubblingRDProcessor : ILadleBubblingRDProcessor
    {
        private readonly ILadleBubblingRepository _ladleBubblingRepository;
        private static MLJSystems.Pi.PI _pi;

        public LadleBubblingRDProcessor(
            IDbConnectionFactory dbConnectionFactory,
            ILadleBubblingRepository ladleBubblingRepository)
        {
            _ladleBubblingRepository = ladleBubblingRepository;

            try
            {
                if (_pi == null)
                    _pi = new PI("ps_pi");

                Log.Information("Connected to PS PI.. ok");
            }
            catch
            {
                // If we can't connect to PI this service won't do much, 
                // but we can keep running and trying to reconnect. 
                // We will continue to connect when we try to UpdatePi below. 
                Log.Error("Unable to connect to PI Server in PiService constructor.");
            }
        }

        public bool IsConnected()
        {
            if (_pi == null)
                _pi = new PI("ps_pi");

            return _pi != null;
        }

        private LadleBubblingParams rdTags = new LadleBubblingParams
        {
            Unit = LadleUnit.RD_KTB_LOCATION,
            AddedTreatmentSeconds = 0,
            CouplerEngagedTag = "RD Coupler_Engaged",
            C1FlowTag = "RD_Coupler_Line 1 Actual Flow",
            C2FlowTag = "RD_Coupler_Line 2 Actual Flow",
            C1PressureTag = "RD_Coupler_Line 1 Back Pressure",
            C2PressureTag = "RD_Coupler_Line 2 Back Pressure",
            HeatTag = "X_HEAT_RD",
            LadleTag = "X_LADLE_RD",
            C1FlowSetpointTag = "RD_Coupler_Line 1 Flow Setpoint",
            C2FlowSetpointTag = "RD_Coupler_Line 2 Flow Setpoint",
            Treatment1FilterExp = "('RD_Coupler_Line 1 Flow Setpoint'>25 or 'RD_Coupler_Line 2 Flow Setpoint'>25)",
            Treatment2FilterExp = "('RD_Coupler_Line 1 Flow Setpoint'<20 and 'RD_Coupler_Line 2 Flow Setpoint'<20)",
            MinimumFlow = 20f,
            MinimumBackPressure = 8f,
            MinimumFlowAfterAllocatedTime = 15f,
        };
        
        public async Task Process()
        {
            // set up tag names and constants for each unit             

            // calculate the ladle bubbling at each unit.
            var rdList = await CalculateLadlesBubblingAtUnit(_pi, rdTags);          

            await SaveLadleBubbling(rdList);            
        }

        /// <summary>
        /// Calculate the ladle bubbling at each unit.
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="ladleParams"></param>
        private async Task<List<Bosplant.Core.Model.PSMET.LadleBubbling>> CalculateLadlesBubblingAtUnit(PI pi, LadleBubblingParams ladleParams)
        {
            // get last date recorded
            var ladleBubbling = await _ladleBubblingRepository.GetLastLadleBubblingAtUnit(ladleParams.Unit);

            var startDate = new DateTime(2018, 09, 09, 07, 0, 0);// ladleBubbling.EventTime ?? DateTime.Now.AddDays(-1);
            var endDate = new DateTime(2018, 09, 16, 19, 0, 0);

            var ladlesBubbling = new List<Bosplant.Core.Model.PSMET.LadleBubbling>();
            Core.Model.PSMET.LadleBubbling staging;
            int bubblingCounter = 0, lastBubblingCounter = 0, connectionCounter = 0, lastConnectionCounter = 0, counter = 0, lastCounter = 0;

            var couplersEngaged = pi.GetRecordedValuesByInside(ladleParams.CouplerEngagedTag, startDate, endDate);

            if (couplersEngaged.Count < 2)
                return ladlesBubbling;

            for (int i = 0; i < couplersEngaged.Count - 1; i++)
            {
                staging = new Core.Model.PSMET.LadleBubbling
                {
                    Unit = ladleParams.Unit,
                    // f1 
                    IsCouplerEngaged = couplersEngaged[i].Bool,
                    // e1
                    EventTime = couplersEngaged[i].Timestamp,
                    BubblingCounter = lastBubblingCounter,
                    //ConnectionCounter = lastConnectionCounter,
                    Counter = lastCounter,
                    IsTreated = (couplersEngaged[i].Bool && !couplersEngaged[i + 1].Bool) // coupler is engaged and next coupler is not engaged
                };

                // do not save if is coupler is engaged
                if (!staging.IsCouplerEngaged)
                    continue;

                if (staging.IsTreated)
                {
                    var engagedFrom = couplersEngaged[i].Timestamp;
                    var engagedTo = couplersEngaged[i + 1].Timestamp;

                    // g1
                    staging.CouplerEngagedDuration = engagedTo.Subtract(engagedFrom).TotalMinutes;

                    // C1 flow
                    // h1
                    staging.C1ActualFlow = pi.GetRecordedValuesByInside(ladleParams.C1FlowTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // Flow after 1 min
                    // p1
                    staging.FlowAfterAllocatedTime = pi.GetFloatArchive(ladleParams.C1FlowTag, engagedFrom.AddMinutes(1));

                    // C2 flow
                    // i1
                    staging.C2ActualFlow = pi.GetRecordedValuesByInside(ladleParams.C2FlowTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // c1 back pressure
                    // j1
                    staging.C1BackPressure = pi.GetRecordedValuesByInside(ladleParams.C1PressureTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get c2 back pressure average
                    // k1
                    staging.C2BackPressure = pi.GetRecordedValuesByInside(ladleParams.C2PressureTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get C1 flow setpoint average
                    staging.C1Setpoint = pi.GetRecordedValuesByInside(ladleParams.C1FlowSetpointTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get C2 flow setpoint average
                    staging.C2Setpoint = pi.GetRecordedValuesByInside(ladleParams.C2FlowSetpointTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // for vessel 1 and 2 get the heat and ladle details
                    // n1
                    staging.HeatNumber = pi.GetIntArchive(ladleParams.HeatTag, engagedFrom);                    

                    // o1
                    staging.LadleNumber = pi.GetIntArchive(ladleParams.LadleTag, engagedFrom);

                    if (staging.CouplerEngagedDuration != null)
                    {
                        // q1
                        staging.Treatment1 =
                            pi.GetRecordedValuesByCount(ladleParams.C1FlowSetpointTag, engagedFrom, ladleParams.Treatment1FilterExp, false, 10)
                                .OrderBy(s => s.Timestamp)
                                .Select(s => s.Timestamp)
                                .FirstOrDefault();
                    }

                    if (staging.Treatment1 != null)
                    {
                        // s1
                        staging.Treatment2 =
                            pi.GetRecordedValuesByCount(ladleParams.C1FlowSetpointTag, staging.Treatment1.Value, ladleParams.Treatment2FilterExp, false, 10)
                                .OrderBy(s => s.Timestamp)
                                .Select(s => s.Timestamp)
                                .FirstOrDefault();
                        // u1
                        staging.TreatmentDuration = (staging.Treatment2.Value - staging.Treatment1.Value).TotalMinutes;

                        // v1
                        staging.AverageFlow = pi.GetRecordedValuesByInside(ladleParams.C1FlowTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();

                        // w1
                        staging.Setpoint = pi.GetRecordedValuesByInside(ladleParams.C1FlowSetpointTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();

                        // x1
                        staging.BackPressure = pi.GetRecordedValuesByInside(ladleParams.C1PressureTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();
                    }

                    staging.HeatNumber = pi.GetIntArchive(ladleParams.HeatTag, engagedFrom);

                    if (staging.CouplerEngagedDuration > 0)
                    {
                        staging.LadleNumber = pi.GetIntArchive(ladleParams.LadleTag, engagedFrom);
                    }

                    if (staging.HeatNumber != null)
                    {
                        if (staging.TreatmentDuration > 0 && staging.HeatNumber != null)
                        {
                            counter++;
                        }
                        staging.Counter = counter;

                        lastBubblingCounter = bubblingCounter;
                        lastConnectionCounter = connectionCounter;
                        lastCounter = counter;

                        staging.IsValidHeat = (
                            staging.AverageFlow > ladleParams.MinimumFlow &&
                            staging.BackPressure > ladleParams.MinimumBackPressure &&
                            staging.FlowAfterAllocatedTime > ladleParams.MinimumFlowAfterAllocatedTime);

                        if (staging.IsValidHeat)
                            staging.GoodBubble = staging.HeatNumber;
                    }
                }
                ladlesBubbling.Add(staging);
            }
            return ladlesBubbling;
        }

        private async Task SaveLadleBubbling(List<Bosplant.Core.Model.PSMET.LadleBubbling> ladleBubbling)
        {            
            await _ladleBubblingRepository.InsertAsync(ladleBubbling);
        }
    }
}
