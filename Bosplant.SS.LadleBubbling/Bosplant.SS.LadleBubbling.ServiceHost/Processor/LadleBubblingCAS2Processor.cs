﻿using Bosplant.Core.Enums;
using Bosplant.Data.Interfaces;
using Bosplant.Data.Interfaces.PSMET;
using Bosplant.SS.LadleBubbling.ServiceHost.DTO;
using MLJSystems.Pi;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosplant.SS.LadleBubbling.ServiceHost.Processor
{
    public class LadleBubblingCAS2Processor : ILadleBubblingCAS2Processor
    {
        private readonly ILadleBubblingRepository _ladleBubblingRepository;
        private static MLJSystems.Pi.PI _pi;

        public LadleBubblingCAS2Processor(
            IDbConnectionFactory dbConnectionFactory,
            ILadleBubblingRepository ladleBubblingRepository)
        {
            _ladleBubblingRepository = ladleBubblingRepository;

            try
            {
                if (_pi == null)
                    _pi = new PI("ps_pi");

                Log.Information("Connected to PS PI.. ok");
            }
            catch
            {
                // If we can't connect to PI this service won't do much, 
                // but we can keep running and trying to reconnect. 
                // We will continue to connect when we try to UpdatePi below. 
                Log.Error("Unable to connect to PI Server in PiService constructor.");
            }
        }

        public bool IsConnected()
        {
            if (_pi == null)
                _pi = new PI("ps_pi");

            return _pi != null;
        }

        private LadleBubblingParams cas2Tags = new LadleBubblingParams
        {
            Unit = LadleUnit.CAS_2_LOCATION,
            AddedTreatmentSeconds = -10,
            CouplerEngagedTag = "Cas2_Coupler Engaged",
            C1FlowSetpointTag = "Cas2_Coupler_Line 1 Flow Setpoint",
            C1FlowTag = "Cas2_Coupler_Line_1_Actual_Flow",
            C1PressureTag = "Cas2_Coupler_Line_1_Back_Pressure",
            VesselTiltAngleTag = "",
            C2FlowSetpointTag = "Cas2_Coupler_Line 2 Flow Setpoint",
            C2FlowTag = "Cas2_Coupler_Line_2_Actual_Flow",
            C2PressureTag = "Cas2_Coupler_Line_2_Back_Pressure",
            HeatTag = "X_HEAT_CAS2",
            HeatVesselCarTag = "",
            LadleTag = "X_LADLE_CAS2",
            LanceFlow = "CAS2_N20_R_FI14018",
            Treatment1FilterExp = "('Cas2_Coupler_Line 1 Flow Setpoint'>25 or 'Cas2_Coupler_Line 2 Flow Setpoint'>25)",
            Treatment2FilterExp = "('Cas2_Coupler_Line 1 Flow Setpoint'<20 and 'Cas2_Coupler_Line 2 Flow Setpoint'<20)",
            MinimumAvFlow = 20f,
            MinimumBackPressure = 8f,
        };

        public async Task Process()
        {         
            // set up tag names and constants

            // calculate the ladle bubbling at each unit.

            var cas2List = await CalculateLadlesBubblingAtUnit(_pi, cas2Tags);

            await SaveLadleBubbling(cas2List);            
        }

        /// <summary>
        /// Calculate the ladle bubbling at each unit.
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="ladleParams"></param>
        private async Task<List<Bosplant.Core.Model.PSMET.LadleBubbling>> CalculateLadlesBubblingAtUnit(PI pi, LadleBubblingParams ladleParams)
        {
            // get last date recorded
            var ladleBubbling = await _ladleBubblingRepository.GetLastLadleBubblingAtUnit(ladleParams.Unit);

            // var startDate = ladleBubbling.EventTime ?? DateTime.Now.AddDays(-1);
            // var endDate = DateTime.Now;
            var startDate = new DateTime(2018, 09, 09, 07, 0, 0);// ladleBubbling.EventTime ?? DateTime.Now.AddDays(-1);
            var endDate = new DateTime(2018, 09, 16, 19, 0, 0);

            var ladlesBubbling = new List<Bosplant.Core.Model.PSMET.LadleBubbling>();
            Core.Model.PSMET.LadleBubbling staging;
            int bubblingCounter = 0, lastBubblingCounter = 0, connectionCounter = 0, lastConnectionCounter = 0, counter = 0, lastCounter = 0;

            var couplersEngaged = pi.GetRecordedValuesByInside(ladleParams.CouplerEngagedTag, startDate, endDate);

            if (couplersEngaged.Count < 2)
                return ladlesBubbling;

            for (int i = 0; i < couplersEngaged.Count-1; i++)
            {
                staging = new Core.Model.PSMET.LadleBubbling
                {
                    Unit = ladleParams.Unit,
                    // g1
                    IsCouplerEngaged = couplersEngaged[i].Bool,
                    // f1
                    EventTime = couplersEngaged[i].Timestamp,
                    BubblingCounter = lastBubblingCounter,
                    //ConnectionCounter = lastConnectionCounter,
                    Counter = lastCounter,
                    // h1
                    IsTreated = (couplersEngaged[i].Bool && !couplersEngaged[i + 1].Bool) // coupler is engaged and next coupler is not engaged
                };

                // do not save if is coupler is engaged
                if (!staging.IsCouplerEngaged)
                    continue;

                if (staging.IsTreated)
                {
                    var engagedFrom = couplersEngaged[i].Timestamp;
                    var engagedTo = couplersEngaged[i + 1].Timestamp;
                    // I1
                    staging.CouplerEngagedDuration = engagedTo.Subtract(engagedFrom).TotalMinutes;

                    // C1 flow
                    // J1
                    staging.C1ActualFlow = pi.GetRecordedValuesByInside(ladleParams.C1FlowTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // Flow after 1 min
                    // k1
                    staging.FlowAfterAllocatedTime = pi.GetFloatArchive(ladleParams.C1FlowTag, engagedFrom.AddMinutes(1));

                    // Vessel position min and max
                    if (!string.IsNullOrEmpty(ladleParams.VesselTiltAngleTag))
                    {
                        // l1
                        staging.VesselPositionMin = pi.GetMin(ladleParams.VesselTiltAngleTag, engagedFrom, engagedTo);
                        // m1
                        staging.VesselPositionMax = pi.GetMax(ladleParams.VesselTiltAngleTag, engagedFrom, engagedTo);
                    }

                    // C2 flow
                    // n1
                    staging.C2ActualFlow = pi.GetRecordedValuesByInside(ladleParams.C2FlowTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // c1 back pressure
                    // o1
                    staging.C1BackPressure = pi.GetRecordedValuesByInside(ladleParams.C1PressureTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get c2 back pressure average
                    // p1
                    staging.C2BackPressure = pi.GetRecordedValuesByInside(ladleParams.C2PressureTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get C1 flow setpoint average
                    // q1
                    staging.C1Setpoint = pi.GetRecordedValuesByInside(ladleParams.C1FlowSetpointTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();

                    // get C2 flow setpoint average
                    // q2
                    staging.C2Setpoint = pi.GetRecordedValuesByInside(ladleParams.C2FlowSetpointTag, engagedFrom, engagedTo)
                        .Select(m => m.Float)
                        .DefaultIfEmpty()
                        .Average();


                    if (staging.CouplerEngagedDuration != null)
                    {
                        if (staging.CouplerEngagedDuration > 0.5f)
                        {
                            // w1
                            staging.Treatment1 =
                                pi.GetRecordedValuesByCount(ladleParams.C1FlowSetpointTag, engagedFrom.AddSeconds(ladleParams.AddedTreatmentSeconds), ladleParams.Treatment1FilterExp, false, 10)
                                    .OrderBy(s => s.Timestamp)
                                    .Select(s => s.Timestamp)
                                    .FirstOrDefault();
                        }
                    }

                    if (staging.Treatment1 != null)
                    {
                        // w2
                        staging.Treatment2 =
                            pi.GetRecordedValuesByCount(ladleParams.C1FlowSetpointTag, staging.Treatment1.Value, ladleParams.Treatment2FilterExp, false, 10)
                                .OrderBy(s => s.Timestamp)
                                .Select(s => s.Timestamp)
                                .FirstOrDefault();
                        // aa
                        staging.TreatmentDuration = (staging.Treatment2.Value - staging.Treatment1.Value).TotalMinutes;
                        // ab
                        staging.AverageFlow = pi.GetRecordedValuesByInside(ladleParams.C1FlowTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();

                        // ac
                        staging.Setpoint = pi.GetRecordedValuesByInside(ladleParams.C1FlowSetpointTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();

                        if (!string.IsNullOrEmpty(ladleParams.ValvePosition))
                        { 
                            // ad
                            staging.ValvePosition = pi.GetRecordedValuesByInside(ladleParams.ValvePosition, staging.Treatment1.Value, staging.Treatment2.Value)
                                .Select(m => m.Float)
                                .DefaultIfEmpty()
                                .Average();
                        }
                        // ae
                        staging.BackPressure = pi.GetRecordedValuesByInside(ladleParams.C1PressureTag, staging.Treatment1.Value, staging.Treatment2.Value)
                            .Select(m => m.Float)
                            .DefaultIfEmpty()
                            .Average();

                        staging.LanceFlow = pi.GetMax(ladleParams.LanceFlow, staging.Treatment1.Value, staging.Treatment2.Value);
                    }

                    // for cas1 and cas2 get the heat and ladle
                    if (staging.CouplerEngagedDuration > 2)
                    {
                        staging.HeatNumber = pi.GetIntArchive(ladleParams.HeatTag, engagedFrom);
                    }
                    if (staging.CouplerEngagedDuration > 0)
                    {
                        staging.LadleNumber = pi.GetIntArchive(ladleParams.LadleTag, engagedFrom);
                    }

                    if (staging.HeatNumber != null)
                    {
                        if (staging.TreatmentDuration > 0.5f)
                        {
                            bubblingCounter++;
                        }
                        if (staging.CouplerEngagedDuration > 0.5f)
                        {
                            connectionCounter++;
                        }
                        if (staging.TreatmentDuration > 0.2 && staging.HeatNumber != null)
                        {
                            counter++;
                        }
                        staging.BubblingCounter = bubblingCounter;
                        //staging.ConnectionCounter = connectionCounter;
                        staging.Counter = counter;

                        lastBubblingCounter = bubblingCounter;
                        lastConnectionCounter = connectionCounter;
                        lastCounter = counter;

                        staging.IsValidHeat = (
                            staging.AverageFlow > ladleParams.MinimumAvFlow &&
                            staging.BackPressure > ladleParams.MinimumBackPressure);

                        if (staging.IsValidHeat)
                            staging.GoodBubble = staging.HeatNumber;
                    }
                }
                ladlesBubbling.Add(staging);
            }
            return ladlesBubbling;
        }

        private async Task SaveLadleBubbling(List<Bosplant.Core.Model.PSMET.LadleBubbling> ladleBubbling)
        {            
            await _ladleBubblingRepository.InsertAsync(ladleBubbling);
        }
    }
}
