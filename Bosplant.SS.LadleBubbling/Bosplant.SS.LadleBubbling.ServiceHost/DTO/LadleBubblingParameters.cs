﻿using Bosplant.Core.Enums;
using System;

namespace Bosplant.SS.LadleBubbling.ServiceHost.DTO
{
    public class LadleBubblingParams
    {
        public LadleUnit Unit { get; set; }

        public double AddedTreatmentSeconds { get; set; }

        public string C1FlowSetpointTag { get; set; }
        public string C1FlowTag { get; set; }        
        public string C1PressureTag { get; set; }
        public string C2FlowSetpointTag { get; set; }
        public string C2FlowTag { get; set; }
        public string C2PressureTag { get; set; }
        public string CouplerEngagedTag { get; set; }
        public string HeatTag { get; set; }
        public string HeatVesselCarTag { get; set; }
        public string LadleTag { get; set; }
        public string LanceFlow { get; set; }
        public double? MinimumAvFlow { get; set; }
        public double? MinimumBackPressure { get; set; }
        public double? MinimumFlow { get; set; }
        public double? MinimumFlowAfterAllocatedTime { get; set; }
        public double? MinimumTime { get; set; }
        public double? MinimumTreatmentDuration { get; set; }
        public string Treatment1FilterExp { get; set; }
        public string Treatment2FilterExp { get; set; }
        public string ValvePosition { get; set; }
        public string VesselTiltAngleTag { get; set; }        
    }
}
