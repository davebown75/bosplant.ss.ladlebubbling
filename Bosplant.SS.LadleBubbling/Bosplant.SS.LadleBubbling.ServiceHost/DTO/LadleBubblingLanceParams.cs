﻿using Bosplant.Core.Enums;

namespace Bosplant.SS.LadleBubbling.ServiceHost.DTO
{
    public class LadleBubblingLanceParams
    {
        public LadleUnit Unit { get; set; }
        public string LadleLimitTag { get; set; }
        public string LadleLimitTagFilter { get; set; }
        public string CarAtUnitFilter { get; set; }
        public string FlowTag { get; set; }
        public string FlowTagFilter { get; set; }
        public string HoodDownTag { get; set; }
        public string HoodDownTagFilter { get; set; }
        public string CouplerEngagedTag { get; set; }
        public string CouplerEngagedTagFilter { get; set; }
        public string HeatTag { get; set; }
        public string HeatTagFilter { get; set; }
        public string ActualFlowTag { get; set; }
        public string ActualFlowTagFilter { get; set; }
        public string NextCarPositionTag { get; set; }
        public string NextCarPositionTagFilter { get; set; }
    }
}
