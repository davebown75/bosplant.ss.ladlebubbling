﻿using Bosplant.Core.Utils;
using Bosplant.Core.ViewModels;
using MLJSystems.Pi;
using MLJSystems.Pi.Simulation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosplant.SS.LadleBubbling.Simulator
{
    class Program
    {       
        static void Main(string[] args)
        {
            //var pi = new PI("PS_PI");

            //DataTable cas1EngTagDT, cas1TagsDT, cas2EngTagDT, cas2TagsDT, v1EngTagDT, v1TagsDT, v2EngTagDT, v2TagsDT;

            //// Scenario 1 - one time only creation, this file can then be reused for testing purposes, 
            //// once you have created this file then you can comment this line
            //var interval = 1;
            //var dateFrom = DateTime.Now.AddDays(-1);
            //var dateTo = DateTime.Now;


            //CreateSimulatorDataToFile(pi, cas1EngTag, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("Cas1EngTagFilename"));
            //CreateSimulatorDataToFile(pi, cas1Tags, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("Cas1TagsFilename"));

            //CreateSimulatorDataToFile(pi, cas2EngTag, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("Cas2EngTagFilename"));
            //CreateSimulatorDataToFile(pi, cas2Tags, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("Cas2TagsFilename"));

            //CreateSimulatorDataToFile(pi, v1EngTag, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("V1EngTagFilename"));
            //CreateSimulatorDataToFile(pi, v1Tags, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("V1TagsFilename"));

            //CreateSimulatorDataToFile(pi, v2EngTag, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("V2EngTagFilename"));
            //CreateSimulatorDataToFile(pi, v2Tags, dateFrom, dateTo, interval, CommonHelper.GetAppSetting<string>("V2TagsFilename"));

            //// Scenario 1 - if this one time only file has been created then you can retrieve this file using ConvertCSVtoDataTable
            //cas1EngTagDT = GetTestData("Cas1EngTagFilename");
            //cas1TagsDT = GetTestData("Cas1TagsFilename");

            //cas2EngTagDT = GetTestData("Cas2EngTagFilename");
            //cas2TagsDT = GetTestData("Cas2TagsFilename");

            //v1EngTagDT = GetTestData("V1EngTagFilename");
            //v1TagsDT = GetTestData("V1TagsFilename");

            //v2EngTagDT = GetTestData("V2EngTagFilename");
            //v2TagsDT = GetTestData("V2TagsFilename");

            //// end of Scenario 1
            //*/

            //var startDate = new DateTime(2018, 7, 30, 7, 0, 0);
            //var endDate = new DateTime(2018, 7, 31, 15, 45, 0);

            //// Scenario 2 - Get Pi recorded data
            ///*cas1EngTagDT = SetAndReturnPIRecordedData(pi, cas1EngTag, "Cas1EngTagData", startDate, endDate);
            //cas1TagsDT = SetAndReturnPIRecordedData(pi, cas1Tags, "Cas1TagsData", startDate, endDate);
            //cas2EngTagDT = SetAndReturnPIRecordedData(pi, cas2EngTag, "Cas2EngTagData", startDate, endDate);
            //cas2TagsDT = SetAndReturnPIRecordedData(pi, cas2Tags, "Cas2TagsData", startDate, endDate);
            //v1EngTagDT = SetAndReturnPIRecordedData(pi, v1EngTag, "V1EngTagData", startDate, endDate);
            //v1TagsDT = SetAndReturnPIRecordedData(pi, v1Tags, "V1TagsData", startDate, endDate);
            //v2EngTagDT = SetAndReturnPIRecordedData(pi, v2EngTag, "V2EngTagData", startDate, endDate);
            //v2TagsDT = SetAndReturnPIRecordedData(pi, v2Tags, "V2TagsData", startDate, endDate);*/

            //// Scenario 2 - Get Pi recorded data from file
            //cas1EngTagDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("Cas1EngTagData"));
            //cas1TagsDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("Cas1TagsData"));
            //cas2EngTagDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("Cas2EngTagData"));
            //cas2TagsDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("Cas2TagsData"));
            //v1EngTagDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("V1EngTagData"));
            //v1TagsDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("V1TagsData"));
            //v2EngTagDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("V2EngTagData"));
            //v2TagsDT = CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>("V2TagsData"));
            //// end of Scenario 2

            //// vessel 1

            //if (v1EngTagDT == null)

            //{

            //    Console.WriteLine("No Vessel 1 engaged tag test data found ...");

            //    Console.ReadKey();

            //    return;

            //}

            //// ***** VESSEL 1 *****

            //List<LadleBubblingStaging> v1LadlesBubblingStaging = new List<LadleBubblingStaging>();
            //List<Core.ViewModels.LadleBubbling> LadlesBubbling = new List<Core.ViewModels.LadleBubbling>();

            //var count = v1EngTagDT.Rows.Count;

            //for (int i = 0; i < v1EngTagDT.Rows.Count; i++)

            //{

            //    var ca1CouplerEngaged = Convert.ToBoolean(v1EngTagDT.Rows[i]["CA1_Coupler_Engaged"].ToString());

            //    // check that we're not on the last row and ca1CouplerEngaged
            //    if (i + 1 == count || !ca1CouplerEngaged)
            //        continue;

            //    // todo: check what if the next row is also true???
            //    var nextCa1CouplerEngaged = Convert.ToBoolean(v1EngTagDT.Rows[i + 1]["CA1_Coupler_Engaged"].ToString());

            //    // if(nextCa1CouplerEngaged)

            //    var v1LadleBubblingStaging = new LadleBubblingStaging();
            //    var v1LadleBubbling = new LadleBubbling();

            //    v1LadleBubblingStaging.Timestamp = Convert.ToDateTime(v1EngTagDT.Rows[i]["TimeStamp"].ToString());

            //    var couplerEngagedFrom = Convert.ToDateTime(v1EngTagDT.Rows[i]["TimeStamp"].ToString());
            //    var couplerEngagedTo = Convert.ToDateTime(v1EngTagDT.Rows[i + 1]["TimeStamp"].ToString());

            //    v1LadleBubblingStaging.CouplerEngagedDuration = Convert.ToDateTime(v1EngTagDT.Rows[i + 1]["TimeStamp"].ToString()).Subtract(v1LadleBubblingStaging.Timestamp).TotalMinutes;

            //    // C1 flow
            //    // get average Ca1_Coupler_line_1_Actual_Flow from pi
            //    v1LadleBubblingStaging.C1Flow =
            //    v1TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"));

            //    // Flow after 1 min
            //    v1LadleBubblingStaging.FlowAfter1Min = v1TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") <= v1LadleBubblingStaging.Timestamp.AddMinutes(1))
            //        .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //        .Select(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"))
            //        .First();

            //    // Vessel position min
            //    v1LadleBubblingStaging.VesselPosMin = v1TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Min(r => r.Field<double>("GM02H450"));

            //    // Vessel position max
            //    v1LadleBubblingStaging.VesselPosMax = v1TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Max(r => r.Field<double>("GM02H450"));

            //    // C2 flow
            //    v1LadleBubblingStaging.C2Flow =
            //    v1TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_2_Actual_Flow"));

            //    // C1 back pressure
            //    if(v1LadleBubblingStaging.C1Flow > 5)
            //    { 
            //        v1LadleBubblingStaging.C1Press = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_1_Back_Pressure"));
            //    }

            //    // C2 back pressure
            //    if (v1LadleBubblingStaging.C2Flow > 5)
            //    {
            //        v1LadleBubblingStaging.C2Press = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_2_Back_Pressure"));
            //    }

            //    // C1 sp
            //    v1LadleBubblingStaging.C1SP = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 1 Flow Setpoint"));

            //    // C2 sp
            //    v1LadleBubblingStaging.C2SP = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 2 Flow Setpoint"));

            //    if(v1LadleBubblingStaging.CouplerEngagedDuration > 0.5f)
            //    {

            //        v1LadleBubblingStaging.HeatVessel = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1"))
            //            .First();

            //        v1LadleBubblingStaging.HeatCar = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1_car"))
            //            .First();

            //        if (v1LadleBubblingStaging.HeatCar != null)
            //        {
            //            v1LadleBubblingStaging.HeatNumber = v1LadleBubblingStaging.HeatCar > 0 ? v1LadleBubblingStaging.HeatCar : v1LadleBubblingStaging.HeatVessel;
            //        }

            //        v1LadleBubblingStaging.LadleNumber = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // Ca1_Coupler_Line 1 Flow Setpoint
            //        // Ca1_Coupler_Line 1 Flow Setpoint > 15 or Ca1_Coupler_Line 2 Flow Setpoint > 15

            //        v1LadleBubblingStaging.Treatment1 = v1TagsDT
            //            .AsEnumerable()
            //            .Where(r => 
            //                r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && 
            //                (
            //                    r.Field<int>("Ca1_Coupler_Line 1 Flow Setpoint") > 15 || 
            //                    r.Field<int>("Ca1_Coupler_Line 2 Flow Setpoint") > 15
            //                )
            //            )
            //            .OrderBy(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // bls.NotSure = 

            //    }
            //    v1LadlesBubblingStaging.Add(v1LadleBubblingStaging);

            //}

            //// ***** END OF VESSEL 1 *****

            //// ***** VESSEL 2 *****

            //List<LadleBubblingStaging> v2LadlesBubblingStaging = new List<LadleBubblingStaging>();
            //List<LadleBubbling> v2LadlesBubbling = new List<LadleBubbling>();

            //var count = v2EngTagDT.Rows.Count;

            //for (int i = 0; i < v2EngTagDT.Rows.Count; i++)

            //{

            //    var ca1CouplerEngaged = Convert.ToBoolean(v2EngTagDT.Rows[i]["CA1_Coupler_Engaged"].ToString());

            //    // check that we're not on the last row and ca1CouplerEngaged
            //    if (i + 1 == count || !ca1CouplerEngaged)
            //        continue;

            //    // todo: check what if the next row is also true???
            //    var nextCa1CouplerEngaged = Convert.ToBoolean(v2EngTagDT.Rows[i + 1]["CA1_Coupler_Engaged"].ToString());

            //    // if(nextCa1CouplerEngaged)

            //    var v2LadleBubblingStaging = new LadleBubblingStaging();
            //    var v2LadleBubbling = new LadleBubbling();

            //    v2LadleBubblingStaging.Timestamp = Convert.ToDateTime(v2EngTagDT.Rows[i]["TimeStamp"].ToString());

            //    var couplerEngagedFrom = Convert.ToDateTime(v2EngTagDT.Rows[i]["TimeStamp").ToString());
            //    var couplerEngagedTo = Convert.ToDateTime(v2EngTagDT.Rows[i + 1]["TimeStamp"].ToString());

            //    v2LadleBubblingStaging.CouplerEngagedDuration = 
            //        Convert.ToDateTime(
            //            v2EngTagDT.Rows[i + 1]["TimeStamp"].ToString()
            //        )
            //        .Subtract(v2LadleBubblingStaging.Timestamp)
            //        .TotalMinutes;

            //    // C1 flow
            //    // get average Ca1_Coupler_line_1_Actual_Flow from pi
            //    v2LadleBubblingStaging.C1Flow =
            //    v2TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && 
            //            r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"));

            //    // Flow after 1 min
            //    v2LadleBubblingStaging.FlowAfter1Min = v2TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") <= v2LadleBubblingStaging.Timestamp.AddMinutes(1))
            //        .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //        .Select(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"))
            //        .First();

            //    // Vessel position min
            //    v2LadleBubblingStaging.VesselPosMin = v2TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Min(r => r.Field<double>("GM02H450"));

            //    // Vessel position max
            //    v2LadleBubblingStaging.VesselPosMax = v2TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Max(r => r.Field<double>("GM02H450"));

            //    // C2 flow
            //    v2LadleBubblingStaging.C2Flow =
            //    v2TagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_2_Actual_Flow"));

            //    // C1 back pressure
            //    if (v2LadleBubblingStaging.C1Flow > 5)
            //    {
            //        v2LadleBubblingStaging.C1Press = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_1_Back_Pressure"));
            //    }

            //    // C2 back pressure
            //    if (v2LadleBubblingStaging.C2Flow > 5)
            //    {
            //        v2LadleBubblingStaging.C2Press = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_2_Back_Pressure"));
            //    }

            //    // C1 sp
            //    v2LadleBubblingStaging.C1SP = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 1 Flow Setpoint"));

            //    // C2 sp
            //    v2LadleBubblingStaging.C2SP = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 2 Flow Setpoint"));

            //    if (v2LadleBubblingStaging.CouplerEngagedDuration > 0.5f)
            //    {

            //        v2LadleBubblingStaging.HeatVessel = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1"))
            //            .First();

            //        v2LadleBubblingStaging.HeatCar = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1_car"))
            //            .First();

            //        if (v2LadleBubblingStaging.HeatCar != null)
            //        {
            //            v2LadleBubblingStaging.HeatNumber = v2LadleBubblingStaging.HeatCar > 0 ? v2LadleBubblingStaging.HeatCar : v2LadleBubblingStaging.HeatVessel;
            //        }

            //        v2LadleBubblingStaging.LadleNumber = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // Ca1_Coupler_Line 1 Flow Setpoint
            //        // Ca1_Coupler_Line 1 Flow Setpoint > 15 or Ca1_Coupler_Line 2 Flow Setpoint > 15

            //        v2LadleBubblingStaging.Treatment1 = v2TagsDT
            //            .AsEnumerable()
            //            .Where(r =>
            //                r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom &&
            //                (
            //                    r.Field<int>("Ca1_Coupler_Line 1 Flow Setpoint") > 15 ||
            //                    r.Field<int>("Ca1_Coupler_Line 2 Flow Setpoint") > 15
            //                )
            //            )
            //            .OrderBy(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // bls.NotSure = 

            //    }
            //    v2LadlesBubblingStaging.Add(v2LadleBubblingStaging);
            //}

            //// ***** END OF VESSEL 2 *****
        }

        private static void CalculateLadleBubblingAtUnit(DataTable engTagDT, DataTable tagsDT, string[] engTag, string[] tags)
        { 
            // ***** VESSEL 1 *****

            //List<LadleBubblingStaging> staging = new List<LadleBubblingStaging>();
            //List<LadleBubbling> ladlesBubbling = new List<LadleBubbling>();

            //var count = engTagDT.Rows.Count;

            //for (int i = 0; i< engTagDT.Rows.Count; i++)

            //{

            //    var ca1CouplerEngaged = Convert.ToBoolean(engTagDT.Rows[i]["CA1_Coupler_Engaged"].ToString());

            //    // check that we're not on the last row and ca1CouplerEngaged
            //    if (i + 1 == count || !ca1CouplerEngaged)
            //        continue;

            //    // todo: check what if the next row is also true???
            //    var nextCa1CouplerEngaged = Convert.ToBoolean(engTagDT.Rows[i + 1]["CA1_Coupler_Engaged"].ToString());

            //    // if(nextCa1CouplerEngaged)

            //    var ladleBubblingStaging = new LadleBubblingStaging();
            //    var ladleBubbling = new LadleBubbling();

            //    ladleBubblingStaging.Timestamp = Convert.ToDateTime(engTagDT.Rows[i]["TimeStamp"].ToString());

            //    var couplerEngagedFrom = Convert.ToDateTime(engTagDT.Rows[i]["TimeStamp"].ToString());
            //    var couplerEngagedTo = Convert.ToDateTime(engTagDT.Rows[i + 1]["TimeStamp"].ToString());

            //    ladleBubblingStaging.CouplerEngagedDuration = Convert.ToDateTime(engTagDT.Rows[i + 1]["TimeStamp"].ToString()).Subtract(ladleBubblingStaging.Timestamp).TotalMinutes;

            //    // C1 flow
            //    // get average Ca1_Coupler_line_1_Actual_Flow from pi
            //    ladleBubblingStaging.C1Flow =
            //    tagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"));

            //    // Flow after 1 min
            //    ladleBubblingStaging.FlowAfter1Min = tagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") <= ladleBubblingStaging.Timestamp.AddMinutes(1))
            //        .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //        .Select(r => r.Field<double>("Ca1_Coupler_line_1_Actual_Flow"))
            //        .First();

            //    // Vessel position min
            //    ladleBubblingStaging.VesselPosMin = tagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Min(r => r.Field<double>("GM02H450"));

            //    // Vessel position max
            //    ladleBubblingStaging.VesselPosMax = tagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Max(r => r.Field<double>("GM02H450"));

            //    // C2 flow
            //    ladleBubblingStaging.C2Flow =
            //    tagsDT
            //        .AsEnumerable()
            //        .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //        .Average(r => r.Field<double>("Ca1_Coupler_line_2_Actual_Flow"));

            //    // C1 back pressure
            //    if(ladleBubblingStaging.C1Flow > 5)
            //    { 
            //        ladleBubblingStaging.C1Press = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_1_Back_Pressure"));
            //    }

            //    // C2 back pressure
            //    if (ladleBubblingStaging.C2Flow > 5)
            //    {
            //        ladleBubblingStaging.C2Press = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_line_2_Back_Pressure"));
            //    }

            //    // C1 sp
            //    ladleBubblingStaging.C1SP = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 1 Flow Setpoint"));

            //    // C2 sp
            //    ladleBubblingStaging.C2SP = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && r.Field<DateTime>("TimeStamp") <= couplerEngagedTo)
            //            .Average(r => r.Field<double>("Ca1_Coupler_Line 2 Flow Setpoint"));

            //    if(ladleBubblingStaging.CouplerEngagedDuration > 0.5f)
            //    {

            //        ladleBubblingStaging.HeatVessel = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1"))
            //            .First();

            //        ladleBubblingStaging.HeatCar = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_HEAT_Vessel_1_car"))
            //            .First();

            //        if (ladleBubblingStaging.HeatCar != null)
            //        {
            //            ladleBubblingStaging.HeatNumber = ladleBubblingStaging.HeatCar > 0 ? ladleBubblingStaging.HeatCar : ladleBubblingStaging.HeatVessel;
            //        }

            //        ladleBubblingStaging.LadleNumber = tagsDT
            //            .AsEnumerable()
            //            .Where(r => r.Field<DateTime>("TimeStamp") <= couplerEngagedFrom)
            //            .OrderByDescending(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // Ca1_Coupler_Line 1 Flow Setpoint
            //        // Ca1_Coupler_Line 1 Flow Setpoint > 15 or Ca1_Coupler_Line 2 Flow Setpoint > 15

            //        ladleBubblingStaging.Treatment1 = tagsDT
            //            .AsEnumerable()
            //            .Where(r => 
            //                r.Field<DateTime>("TimeStamp") >= couplerEngagedFrom && 
            //                (
            //                    r.Field<int>("Ca1_Coupler_Line 1 Flow Setpoint") > 15 || 
            //                    r.Field<int>("Ca1_Coupler_Line 2 Flow Setpoint") > 15
            //                )
            //            )
            //            .OrderBy(r => r.Field<DateTime>("TimesStamp"))
            //            .Select(r => r.Field<int>("X_LADLE_Vessel_1_car"))
            //            .First();

            //        // bls.NotSure = 

            //    }
            //    staging.Add(ladleBubblingStaging);
            //}
        }

        private static void CreateSimulatorDataToFile(PI pi, string[] tags, DateTime dateFrom, DateTime dateTo, int interval, string filename)
        {
            //var simulationBuilder = new PISimulationFileBuilder(pi);
           
            //simulationBuilder.Build(tags, DateTime.Now.AddDays(-1), DateTime.Now, 60, filename);
        }

        private static DataTable GetTestData(string filename)
        {
            try
            {
                var filePath = CommonHelper.GetAppSetting<string>(filename);

                return Bosplant.Core.Utils.Serializer.DeSerializeObject<DataTable>(filePath);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Failed to open test data");
                Console.WriteLine("Press any key to exit ...");
                throw ex;
            }
        }

        private static DataTable SetAndReturnPIRecordedData(PI _pi, string[] tags, string filenameKey, DateTime startTime, DateTime endTime)
        {
            try
            {
                var dt = _pi.GetRecordedData(
                    tags,
                    startTime,
                    endTime);

                CommonHelper.WriteCsv(dt, CommonHelper.GetAppSetting<string>(filenameKey));

                return CommonHelper.ConvertCSVtoDataTable(CommonHelper.GetAppSetting<string>(filenameKey));
            }
            catch
            {
                Console.WriteLine("Failed to open test data.");
                Console.WriteLine("Press any key to exit.");
                throw new Exception();
            }
        }
    }
}
